import 'package:flutter/material.dart';

class QuizScreen extends StatefulWidget {
  @override
  _QuizScreenState createState() => _QuizScreenState();
}

class _QuizScreenState extends State<QuizScreen> {
  int _currentIndex = 0;
  List<Map<String, dynamic>> _questions = [
    {
      'question': 'What is the largest planet in the solar system?',
      'answers': ['Jupiter', 'Saturn', 'Earth', 'Mars'],
      'correctAnswer': 'Jupiter',
    },
    {
      'question': 'Who wrote "To Kill a Mockingbird"?',
      'answers': [
        'Harper Lee',
        'Stephen King',
        'J.K. Rowling',
        'Charles Dickens'
      ],
      'correctAnswer': 'Harper Lee',
    },
    {
      'question': 'Which country won the FIFA World Cup in 2018?',
      'answers': ['Brazil', 'Argentina', 'Germany', 'France'],
      'correctAnswer': 'France',
    },
    {
      'question': 'What is the chemical symbol for water?',
      'answers': ['H2O', 'CO2', 'NaCl', 'O2'],
      'correctAnswer': 'H2O',
    },
    {
      'question': 'Who painted the Mona Lisa?',
      'answers': [
        'Leonardo da Vinci',
        'Vincent van Gogh',
        'Pablo Picasso',
        'Michelangelo'
      ],
      'correctAnswer': 'Leonardo da Vinci',
    },
    {
      'question': 'What is the capital of Japan?',
      'answers': ['Tokyo', 'Kyoto', 'Osaka', 'Seoul'],
      'correctAnswer': 'Tokyo',
    },
    {
      'question': 'Which planet is known as the Red Planet?',
      'answers': ['Mars', 'Venus', 'Jupiter', 'Mercury'],
      'correctAnswer': 'Mars',
    },
    {
      'question': 'Who developed the theory of relativity?',
      'answers': [
        'Albert Einstein',
        'Isaac Newton',
        'Galileo Galilei',
        'Stephen Hawking'
      ],
      'correctAnswer': 'Albert Einstein',
    },
  ];
  Map<int, String?> _selectedAnswers = {};

  void _answerQuestion(String? answer) {
    setState(() {
      _selectedAnswers[_currentIndex] = answer;
      if (_currentIndex < _questions.length - 1) {
        _currentIndex++;
      } else {
        _showQuizResult();
      }
    });
  }

  void _showQuizResult() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Quiz Completed'),
          content:
              Text('Your Score: ${_calculateScore()} / ${_questions.length}'),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
                _restartQuiz();
              },
              child: Text('Restart Quiz'),
            ),
          ],
        );
      },
    );
  }

  int _calculateScore() {
    int score = 0;
    for (int i = 0; i < _questions.length; i++) {
      String? selectedAnswer = _selectedAnswers[i];
      String correctAnswer = _questions[i]['correctAnswer'];
      if (selectedAnswer == correctAnswer) {
        score++;
      }
    }
    return score;
  }

  void _restartQuiz() {
    setState(() {
      _currentIndex = 0;
      _selectedAnswers.clear();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Quiz'),
      ),
      body: _currentIndex < _questions.length
          ? QuizQuestion(
              question: _questions[_currentIndex]['question'],
              answers: _questions[_currentIndex]['answers'],
              onAnswerSelected: _answerQuestion,
            )
          : Center(
              child:
                  CircularProgressIndicator(), // Placeholder when the quiz is completed
            ),
    );
  }
}

class QuizQuestion extends StatelessWidget {
  final String question;
  final List<String> answers;
  final Function(String?) onAnswerSelected;

  QuizQuestion({
    required this.question,
    required this.answers,
    required this.onAnswerSelected,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Text(
            question,
            style: TextStyle(fontSize: 20.0),
          ),
        ),
        ...answers.map((answer) {
          return Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
            child: ElevatedButton(
              onPressed: () => onAnswerSelected(answer),
              child: Text(answer),
            ),
          );
        }).toList(),
      ],
    );
  }
}
